import {Component, Property} from '@wonderlandengine/api';
import { HowlerAudioSource } from '@wonderlandengine/components';

/**
 * AudioManager
 */
export class AudioManager extends Component {
  static TypeName = 'AudioManager';
  /* Properties that are configurable in the editor */
  static Properties = {
  };

  static onRegister(engine) {
    /* Triggered when this component class is registered.
      * You can for instance register extra component types here
      * that your component may create. */
    engine.registerComponent(HowlerAudioSource);
  }

  init() {
    console.log('init() with param', this.param);
  }

  start() {
    console.log('start() with param', this.param);
    
    // Setup SFX
    this.sfxStars = this.object.addComponent(HowlerAudioSource, {
        src: 'sfx/at-night-in-ferry.mp3',
        spatial: false,
        volume: 0.150
    });

    // TEST: Play SFX
    // this.sfxStars.play();
  }

  update(dt) {
    /* Called every frame. */
  }
}
