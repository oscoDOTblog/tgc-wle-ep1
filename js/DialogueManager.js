import {
  Component, 
  Property,
  TextComponent,
} 
from '@wonderlandengine/api';

/**
 * DialogueManager
 */
export class DialogueManager extends Component {
  static TypeName = 'DialogueManager';
  /* Properties that are configurable in the editor */
  static Properties = {
    // speakerPlateObject: Property.object(),
    speakerTextObject: Property.object(),
    skyboxMaterial: Property.material(),
    textureBlack: Property.texture(),
    textureBedroom: Property.texture(),
    textureLake: Property.texture(),
    textureHome: Property.texture(),
    textureMisty: Property.texture(),
    textureOffice: Property.texture(),
    textureSpace: Property.texture(),
    textureStars: Property.texture(),
    textureTraffic: Property.texture()
  };

  static onRegister(engine) {
  /* Triggered when this component class is registered.
    * You can for instance register extra component types here
    * that your component may create. */
  }

  init() {
    console.log('init() with param', this.param);

    // Set up lines of dialogue
    this.lines = [
      {
        "speaker": "", 
        "dialogue": "Click Next or Press N to go to Next Page // Click Back or Press B for the Previous Page",
      },
      {
        "speaker": "", 
        "dialogue": "I dream of the next step.",
        "fxAction": "start", 
        "fxSrc": "at-night-in-ferry.mp3",
        "skybox": this.textureStars
      },
      {"speaker": "", "dialogue": "My personal salvation."},
      {"speaker": "", "dialogue": "The answer to my waiting and wandering."},
      {"speaker": "", "dialogue": "In my dream, it’s so close."},
      {
        "speaker": "", 
        "dialogue": "I can almost reach out and grab it-",
        "fxAction": "stop", 
        "fxSrc": "at-night-in-ferry.mp3",
        "skybox": this.textureStars
      },
      {
        "speaker": "", 
        "dialogue": "RING RING RING RING RING",
        "fxAction": "start", 
        "fxSrc": "alarm.mp3",
        "skybox": this.textureBlack
      },
      {"speaker": "", "dialogue": "...",},
      {
        "speaker": "", 
        "dialogue": "Everyday it's the same.", 
        "skybox": this.textureBedroom
      },
      {"speaker": "", "dialogue": "I bolt upright in my bed, startled by the blaring sounds and unsettling vibrations on the floor next to me."},
      {"speaker": "", "dialogue": "A moment later, I assimilate myself into reality, and look over at the innocent alarm clock shrieking below me."},
      // {"speaker": "", "dialogue": "TODO: Add phone turn off interaction"},
      {
        "speaker": "", 
        "dialogue": "I quietly sigh in frustration yet again",
        "fxAction": "alarm.mp3", 
        "fxSrc": "stop",
        "skybox": this.textureBedroom
      },
      {
        "speaker": "", 
        "dialogue": "<<SOMETIME IN THE PAST>>", 
        "skybox": this.textureBlack
      },
      {"speaker": "", "dialogue": "It's been two years since the start of my corporate job."},
      {"speaker": "", "dialogue": "For all intents and purposes, I should be happy."},
      {"speaker": "", "dialogue": "I spent all my youth becoming a model student, grinding my ass off to keep that streak up in college, and was lucky enough to land a six-figure job right out of college."},
      {"speaker": "", "dialogue": "'Lucky me!', I initially thought."},
      {"speaker": "", "dialogue": "Oh, if only that naivety lasted for more than just one moment."},
      {
        "speaker": "", 
        "dialogue": "...", 
        "skybox": this.textureBlack
      },
      {
        "speaker": "", 
        "dialogue": "My only goal was the future, and I would always give up today to reach it.",
        "skybox": this.textureMisty,
        "fxAction": "start",
        "fxSrc": "anxiety.mp3"
      },
      {"speaker": "", "dialogue": "As a kid, I would dismiss any invitations to hang out, as I needed the extra time to study."},
      {
        "speaker": "", 
        "dialogue": "In college, I did the same but with clubs and other extracurriculars - if it did not have anything to do with my future, I wanted no part in it.",
        "skybox": this.textureMisty
      },
      {
        "speaker": "", 
        "dialogue": "...", 
        "skybox": this.textureBlack
      },
      {
        "speaker": "", 
        "dialogue": "When I finally got the job I was seeking, my sole focus had now been to pay off my student loans.",
        "fxAction": "start",
        "fxSrc": "office.mp3",
        "skybox": this.textureOffice
      },
      {"speaker": "", "dialogue": "To speed up the repayment process, I decided against leasing an apartment, even though it would put me closer to work."},
      {"speaker": "", "dialogue": "I liked driving at the time, so what was the big deal taking a couple hours a month to get to work?"},
      {
        "speaker": "", 
        "dialogue": "If only I could be ever more wrong.",
        "skybox": this.textureOffice
      },
      {
        "speaker": "", 
        "dialogue": "...", 
        "skybox": this.textureBlack
      },
      {
        "speaker": "", 
        "dialogue": "It was 2-3 hours to get to work, and 2-3 more to get back. Quickly compound that by 5 times a week, for 52 weeks straight, and that's a recipe for depression",
        "fxAction": "start",
        "fxSrc": "traffic.mp3",
        "skybox": this.textureTraffic
      },
      {"speaker": "", "dialogue": "As soon as I overcame the daily battle just to get into the office, I would immediately get belittled by whatever manager of the month I was assigned."},
      {"speaker": "", "dialogue": "They would tell me I wasn't pulling my weight on the team, not living the corporate values, and forcing me to spend even longer hours in the office just to meet the bare minimum expectations."},
      {
        "speaker": "", 
        "dialogue": "Quietly I would grind my teeth and keep my head down. It's to pay off my student loans, and once I do that, I can finally start my real life....",
        "skybox": this.textureTraffic
      },
      {
        "speaker": "", 
        "dialogue": "...", 
        "fxAction": "stop", 
        "fxSrc": "traffic.mp3",
        "skybox": this.textureBlack
      },
      {
        "speaker": "", 
        "dialogue": "This daily cycle would persist until my sanity would wane and every ounce of compassion was ground away into fine particles, leaving all semblance of my humanity in the memories of yesterday",
        "fxAction": "start",
        "fxSrc": "anxiety.mp3",
        "skybox": this.textureMisty
      },
      {"speaker": "", "dialogue": "It was then I also learned that you can scream as loud as you want inside your car in the corporate parking lot."},
      {"speaker": "", "dialogue": "It didn't help, so I wondered why I even kept doing it."},
      {
        "speaker": "", 
        "dialogue": "Maybe, if just for a moment, I could release all the frustration and anger that had been building, even though it would come back twice as prevalent the next day.", 
        "skybox": this.textureMisty
      },
      {
        "speaker": "", 
        "dialogue": "...", 
        "fxAction": "stop", 
        "fxSrc": "anxiety.mp3",
        "skybox": this.textureBlack
      },
      {
        "speaker": "", 
        "dialogue": "The only solace on those long days would be the end of them.",
        "fxAction": "start", 
        "fxSrc": "crickets.mp3",
        "skybox": this.textureHome
      },
      {"speaker": "", "dialogue": "Coming home around 9PM on average, my beater of a car would go though various twists and turns and find itself in a quiet suburban neighborhood at the edge of a no-name city."},
      {"speaker": "", "dialogue": "Fuming about that day's injustices, I would uneagerly stumble out of the car, grab my work bag, lock the car door, and then look up."},
      {
        "speaker": "", 
        "dialogue": "And for just a moment, I would find serenity.", 
        "skybox": this.textureHome
      },
      {
        "speaker": "", 
        "dialogue": "...", 
        "fxAction": "stop", 
        "fxSrc": "crickets.mp3",
        "skybox": this.textureBlack
      },
      {
        "speaker": "", 
        "dialogue": "In busy cities and homesteads, you would never even see this.",
        "fxAction": "start", 
        "fxSrc": "wonder.mp3",
        "skybox": this.textureStars
      },
      {"speaker": "", "dialogue": "But here at what seems like the edge of the world, they eagerly awaited my return every night."},
      {"speaker": "", "dialogue": "Stars would dance and twinkle across the night sky as if going on a journey, and inviting me to come along with them."},
      {
        "speaker": "", 
        "dialogue": "Unconsciously, I would reach out with my free hand and try to accept their amazing proposal, too pure and too beautiful for an unremarkable nobody like me.",
        "skybox": this.textureStars
      },
      // {"speaker": "", "dialogue": "TODO: INT -> Touch Start"},
      {
        "speaker": "", 
        "dialogue": "When my arm fully extended and I tried to clasp onto their hands-",
        "fxAction": "stop", 
        "fxSrc": "wonder.mp3",
        "skybox": this.textureBlack
      },
      // {"speaker": "", "dialogue": "Play FX: Glass Shattering"},
      {
        "speaker": "", 
        "dialogue": "The immersion would break apart.", 
        "fxAction": "start", 
        "fxSrc": "crickets.mp3",
        "skybox": this.textureHome
      },
      // {"speaker": "", "dialogue": "TODO: INT -> Balled Up Fist Glowing"},
      // {"speaker": "", "dialogue": "Play FX: Shine"},
      {"speaker": "", "dialogue": "Slowly retracting my hand back, I would look at my balled up fist, hoping that maybe this time, I would find something that could finally change my life, something to take away me from this monotone reality that I had been incarcerated into."},
      // {"speaker": "", "dialogue": "TODO: INT -> Open Hand, Glow Goes Away"},
      {"speaker": "", "dialogue": "And yet again, I would find nothing."},
      {"speaker": "", "dialogue": "A quiet sigh would escape my lips, and then I would slowly begin the trudge up the hill to return to the quiet old home that I had looked upon thousands, if not millions, of times."},
      {"speaker": "", "dialogue": "It was as if the house silently judged me, a reminder that I was still here, still waiting, for the next part of my life to be given to me, and everyday it would remind me that I had failed yet again."},
      {
        "speaker": "", 
        "dialogue": "No matter how much I wanted to be with them, the stars would always live just beyond my reach, and they would always stay in a reality beyond my own.",
        "skybox": this.textureHome
      },
      {
        "speaker": "", 
        "dialogue": "...", 
        "fxAction": "stop", 
        "fxSrc": "crickets.mp3",
        "skybox": this.textureBlack
      },
      {
        "speaker": "", 
        "dialogue": "<<THE PRESENT>>", 
      },
      {
        "speaker": "", 
        "dialogue": "Eventually, I became free of my student loans, and I decided that maybe, the next step was to move somewhere closer to my peers.",
      },
      {
        "speaker": "", 
        "dialogue": "...", 
        "skybox": this.textureBlack
      },
      {
          "speaker": "",
          "dialogue": "I found a cheap apartment near my office, and an emotion I thought I had lost began bubbling underneath the surface: excitement.",
          "fxAction": "start", 
          "fxSrc": "condo.mp3",
          "skybox": this.textureBedroom
      },
      {
          "speaker": "",
          "dialogue": "No more road rage, no more driving, no more long hours to and from work, and most importantly, no more saying no to social invitations."
      },
      {
          "speaker": "",
          "dialogue": "Maybe I was wrong for having spent all my focus towards paying off my debt, as I had nothing to show for it but one less expense in my life."
      },
      {
        "speaker": "", 
        "dialogue": "...", 
        "skybox": this.textureBlack
      },
      {
          "speaker": "",
          "dialogue": "Unabatedly excited to begin my life as a new man, I took my first steps in the office and was told to go home forever.",
          "fxAction": "stop", 
          "fxSrc": "condo.mp3",
          "skybox": this.textureOffice
      },
      {
        "speaker": "", 
        "dialogue": "...", 
        "skybox": this.textureBlack
      },
      {
          "speaker": "",
          "dialogue": "For weeks prior, there had been news of a deadly disease spreading in a place far across the ocean and far away from anything relating to me, until it finally did.",
          "fxAction": "start", 
          "fxSrc": "space.mp3",
          "skybox": this.textureSpace
      },
      {
          "speaker": "",
          "dialogue": "Each day the disease would spread across people, across cities, across borders and across continents, and finally the strain had come to my own little office park.",
          "skybox": this.textureSpace
      },
      {
        "speaker": "", 
        "dialogue": "...", 
        "fxAction": "stop", 
        "fxSrc": "space.mp3",
        "skybox": this.textureBlack
      },
      {
          "speaker": "",
          "dialogue": "The company decided in the best interests of employee health to shut down all its physical locations, and I was promptly told to go home until further notice.",
          "fxAction": "start", 
          "fxSrc": "office.mp3",
          "skybox": this.textureOffice
      },
      {
        "speaker": "", 
        "dialogue": "...", 
        "fxAction": "stop", 
        "fxSrc": "office.mp3",
        "skybox": this.textureBlack
      },
      {
        "speaker": "", 
        "dialogue": "When I selected my new apartment, I did it only so with the intent of sleeping there, nothing more.",
        "skybox": this.textureBedroom
      },
      {"speaker": "", "dialogue": "I had no intention of furnishing it because buying and moving stuff was a hassle - I've helped more than one family member move their artifacts of old to new homes, and more than once was enough for me."},
      {"speaker": "", "dialogue": "Once I turned the key and opened my apartment, all that awaited me was a crowded hallway poorly masquerading as an apartment."},
      {"speaker": "", "dialogue": "I didn't even have a desk to work at because I expected to spend all my time in the office."},
      {"speaker": "", "dialogue": "As we were told to work from home until further notice, the bareness and solitude of my new home began to gnaw at my loneliness."},
      {"speaker": "", "dialogue": "I could always go back home with my tail between my legs, but my ego and my fear of breaking the lease early would not allow that."},
      {"speaker": "", "dialogue": "Looking around the room for what I could make into a makeshift desk, I found the luggage container I used to bring in a small set of clothes, and an empty box from the new backpack I bought with the intention of starting my new office life."},
      // {"speaker": "", "dialogue": "TODO: INT -> Move Luggage and Box into Desk"},
      {"speaker": "", "dialogue": "I flipped the luggage container, stacked the box on top of it, and then finally rested my expensive corporate laptop on it."},
      {"speaker": "", "dialogue": "I would have to sit on the floor for this setup to work, but something was better than nothing, I suppose."},
      // {"speaker": "FX", "dialogue": "Box Tearing"},
      {"speaker": "", "dialogue": "As I leaned forward to place my hands on the keyboard and test my setup, my weight crushed the cardboard box and careened me forward as the support I was expecting instantly disappeared from beneath me."},
      // {"speaker": "FX", "dialogue": "***THUD***"},
      {"speaker": "", "dialogue": "My head slammed against the wall before me, and tears began to well in my eyes as I made no effort to get up."},
      {"speaker": "", "dialogue": "Gone were days I spent working relentlessly to get to the point I was at today."},
      {"speaker": "", "dialogue": "Gone were the friends and family I had because I believed relationships were too much maintenance and a constant distraction for the goals I was trying to achieve."},
      {"speaker": "", "dialogue": "Gone were the opportunities I deserved after a lifetime of sacrifice because of a pandemic that had no place ruining my life."},
      {"speaker": "", "dialogue": "My whole body drooped forward until it found itself completely against the wall, slumped over the crushed box and the now dented corporate laptop."},
      {"speaker": "", "dialogue": "I wanted to ask myself if this was really all that was awaiting for me until \"further notice\", but I was too scared to even ponder that possibility."},
      {
        "speaker": "", 
        "dialogue": "And so I laid there for a nondescript amount of time, afraid to wonder if there would even be a tomorrow for me.",
        "skybox": this.textureBedroom
      },
      {
        "speaker": "", 
        "dialogue": "...", 
        "skybox": this.textureBlack
      },
      {
        "speaker": "Osco", 
        "dialogue": "Thank you so much for checking out Episode Zero of <<The Grand Challenge>>!",
        "fxAction": "start", 
        "fxSrc": "sleepywood.mp3",
        "skybox": this.textureLake
      },
      {"speaker": "Osco", "dialogue": "If yWou would like to keep up with upcoming episode releases, consider signing up for our newsletter on the next page!"},
      {"speaker": "Osco", "dialogue": "As thanks for getting to the end, you will earn a digital badge! This will unlock cool experiences in the future..."},
      {"speaker": "Osco", "dialogue": "Be of good cheer, and I'll see you at the next episode release <3 - Osco"}
      // TODO: Redirect user to newsletter page
    ]

    // Set Dialogue Control State
    this.index = 0
    this.numberOfLines = this.lines.length
    console.log("numberOfLines" + this.numberOfLines)

    // Setup Desktop Controls State
    this.held = false; // Boolean: Set to true after intial keydown, set back to false after keyup
    this.help = false // Help: Opens and closes help diagram
    this.next = false; // Next: Goes to Next dialogue
    this.prev = false; // Prev: Goes to Previous dialogue

    // Bind Event Listeners to Key Presses
    window.addEventListener('keydown', this.keyPress.bind(this));
    window.addEventListener('keyup', this.keyRelease.bind(this));

    // TEST
    console.log("adding dialogue manager!")
  }

  start() {
    console.log('start() with param', this.param);

    // TODO: Get Speaker Plate Component

    // Set Speaker Text Component
    this.speakerText = this.speakerTextObject.getComponent(TextComponent);
  }

  update(dt) {
    /* Called every frame. */
    // Update Based on Prev and Next Key Presses
    if (this.help && this.held) {
      console.log("Help!")
      this.help = false // Must press each time, no long holds
    }
    if (this.next && this.held) {
      console.log("Next!")
      this.next = false // Must press each time, no long holds
      this.setDialogueText("next") // Go to next dialogue line

      // TEST: Set Text Object
      // this.speakerText.text = "Pizza Time"
    }
    if (this.prev && this.held) {
      console.log("Prev!")
      this.prev = false // Must press each time, no long holds
      this.setDialogueText("prev") // Go to next dialogue line
    }
  }

  // Keydown Functions
  keyPress(e) {
    // console.log(e)
    // H = Help if not currently being held down
    if (!this.held && e.keyCode === 72 /* H */ || e.keyCode === "KeyH" /* H */) {
      this.held = true
      this.help = true
    } 

    // N = Next if not currently being held down
    else if (!this.held && e.keyCode === 78 /* N */ || e.keyCode === "KeyN" /* N */) {
      this.held = true
      this.next = true
    } 

    // P = Prev if not currently being held down
    else if (!this.held && e.keyCode === 80 /* P */ || e.keyCode === "KeyP" /* P */) {
      this.held = true
      this.prev = true
    } 
  }

  // Keyup Functions
  keyRelease(e) {
    // H = Help if not currently being held down
    if (!this.held && e.keyCode === 72 /* H */ || e.keyCode === "KeyH" /* H */) {
      this.held = false
      this.help = false
    } 

    // N = Next and reset held
    else if (e.keyCode === 78 /* N */ || e.keyCode === "KeyN" /* N */) {
      this.held = false
      this.next = false
    }

    // P = Prev and reset held
    else if (e.keyCode === 80 /* P */ || e.keyCode === "KeyP" /* P */) {
      this.held = false
      this.prev = false
    } 
  }

  // Get Dialogue from Line Index
  getDialogue(index) {
    return this.lines[index].dialogue
  }

  // Get Skybox from Line Index, if it exists 
  getSkybox(index) {
    if (this.lines[index].skybox) {
      return this.lines[index].skybox
    }
    else {
      return null
    } 
  }

  // Get Speaker from Line Index
  getSpeaker(index) {
    return this.lines[index].speaker
  }

  // Set Dialogue Text
  setDialogueText(direction) {

    // Render the Next Line
    if (direction == "next"){
      // Do not go over maximum amount of lines 
      // TODO: Redirect to sign up page :^)
      if ((this.index + 1) <= this.numberOfLines){
        this.index++
      }
    }

    // Render the Previous Line
    if (direction == "prev"){
      // Do not go before the first line of dialogue
      if ((this.index - 1) >= 0){
        this.index--
      }
    }

    // TODO: Hide speaker plate if character is monologuing
    // let speaker = this.getSpeaker(index)
    // this.setSpeakerPlateVisibility(speaker)

    // TODO: Set Speaker Plate
    // Set text values for speaker, dialogue, and sound effects
    // speakerTextEl.setAttribute('text', 'value', speaker); // Set Speaker Plate
    // this.speakerPlate.text = 

    // Set Speaker Text
    // dialogueTextEl.setAttribute('text', 'value', getDialogue(index)); 
    console.log(this.getDialogue(this.index))
    this.speakerText.text = this.getDialogue(this.index)

    // Set skybox
    if (this.getSkybox(this.index)){ // Set Skybox, if it is required
      this.skyboxMaterial.texture = this.getSkybox(this.index)
    }

    // Set or unset Audio (fx)
    // fxBgmEl.components.sound.playSound();
  }

  // TODO: Shows or Hides Speaker Plate
  setSpeakerPlateVisibility(speaker) {
    if (speaker){
      this.speakerPlateEl.object3D.visible = true;
    }
    else {
      this.speakerPlateEl.object3D.visible = false;
    }
  }
}
